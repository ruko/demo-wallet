LINTER_CONFIG:=.golangci.pipeline.yaml

.PHONY: .install-lint
.install-lint:
ifeq ($(wildcard $(shell which golangci-lint)),)
	$(info Downloading golangci-lint)
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.45.2
endif

.PHONY: lint
lint: .install-lint
	golangci-lint run --config=${LINTER_CONFIG} ./...
