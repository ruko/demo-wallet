package grpc

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
	wallet "gitlab.ru/rkosykh/demo-wallet/test/utils/clients/http"
)

func TestGetAccountBalance(t *testing.T) {
	t.Run("Positive cases", func(t *testing.T) {
		for _, tt := range []struct {
			name    string
			balance int32
		}{
			{
				name:    "positive balance",
				balance: 100,
			}, {
				name:    "negative balance",
				balance: -100,
			}, {
				name:    "zero balance",
				balance: 0,
			},
		} {
			t.Run(tt.name, func(t *testing.T) {
				ctx := context.Background()

				account, err := mustCreateAccount(ctx, tt.balance)
				require.NoError(t, err)

				res, resp, err := client.GetAccountBalance(ctx, &wallet.GetAccountBalanceRequest{
					AccountID: account.AccountID,
				})
				require.NoError(t, err)
				require.Equal(t, http.StatusOK, resp.StatusCode)
				require.NotNil(t, resp)
				require.Equal(t, res.AccountID, account.AccountID)
				require.Equal(t, res.Amount, account.Amount)
			})
		}

	})
}
