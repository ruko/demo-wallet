package grpc

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetBalance получить инфу по балансу
func TestGetAccountBalance(t *testing.T) {
	t.Run("Positive cases", func(t *testing.T) {
		for _, tt := range []struct {
			name    string
			balance int32
		}{
			{
				name:    "positive balance",
				balance: 100,
			}, {
				name:    "negative balance",
				balance: -100,
			}, {
				name:    "zero balance",
				balance: 0,
			},
		} {
			t.Run(tt.name, func(t *testing.T) {
				ctx := context.Background()

				account, err := mustCreateAccount(ctx, tt.balance)
				require.NoError(t, err)

				balance, err := walletClient.GetAccountBalance(ctx, &desc.GetAccountBalanceRequest{
					AccountId: account.AccountID,
				})
				require.NoError(t, err)
				require.NotNil(t, balance)
				require.Equal(t, account.AccountID, balance.AccountId)
				require.Equal(t, account.Amount, balance.Amount)
			})
		}

	})

	t.Run("Negative cases", func(t *testing.T) {
		t.Run("Account doesn't exists", func(t *testing.T) {
			ctx := context.Background()

			balance, err := walletClient.GetAccountBalance(ctx, &desc.GetAccountBalanceRequest{
				AccountId: uuid.NewString(),
			})
			require.EqualError(t, err, status.Error(codes.Internal, "Get balance err: account not found").Error())
			require.Nil(t, balance)
		})
	})
}
