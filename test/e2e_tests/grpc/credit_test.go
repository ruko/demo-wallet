package grpc

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/domain"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestCredit(t *testing.T) {
	t.Run("Positive cases", func(t *testing.T) {
		ctx := context.Background()

		account, err := mustCreateAccount(ctx, 100)
		require.NoError(t, err)

		creditRequest := &desc.CreditRequest{
			AccountId:   account.AccountID,
			Amount:      100,
			OperationId: uuid.NewString(),
		}

		resp, err := walletClient.Credit(ctx, creditRequest)
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.Equal(t, desc.OperationStatus_STATUS_OK, resp.Status)

		operations, err := store.GetAccountOperations(ctx, account.AccountID, 1000)
		require.NoError(t, err)
		require.Len(t, operations, 1)

		require.Equal(t, creditRequest.Amount, operations[0].Amount)
		require.Equal(t, creditRequest.OperationId, operations[0].OperationID)
		require.Equal(t, domain.OperationTypeCredit, operations[0].OperationType)

		checkNewAccountBalance(ctx, t, walletClient, account, creditRequest.Amount, domain.OperationTypeCredit)
	})

	t.Run("Negative cases", func(t *testing.T) {
		t.Run("Account doesn't exists", func(t *testing.T) {
			ctx := context.Background()

			accountID := uuid.NewString()
			resp, err := walletClient.Credit(ctx, &desc.CreditRequest{
				AccountId:   uuid.NewString(),
				Amount:      100,
				OperationId: uuid.NewString(),
			})
			require.EqualError(t, err, status.Error(codes.Internal, "Credit process err: can't get account balance for change amount: account not found").Error())
			require.Nil(t, resp)

			checkOperationNotExists(ctx, t, accountID)
		})

		t.Run("Double operation with same ID", func(t *testing.T) {
			ctx := context.Background()

			account, err := mustCreateAccount(ctx, 100)
			require.NoError(t, err)

			creditRequest := &desc.CreditRequest{
				AccountId:   account.AccountID,
				Amount:      100,
				OperationId: uuid.NewString(),
			}

			t.Log("First credit operation")

			resp, err := walletClient.Credit(ctx, creditRequest)
			require.NoError(t, err)
			require.NotNil(t, resp)
			require.Equal(t, desc.OperationStatus_STATUS_OK, resp.Status)

			t.Log("Second credit operation")

			resp, err = walletClient.Credit(ctx, creditRequest)

			require.Error(t, err)
			require.Nil(t, resp)

			operations, err := store.GetAccountOperations(ctx, account.AccountID, 1000)
			require.NoError(t, err)
			require.Len(t, operations, 1)
		})
	})
}
