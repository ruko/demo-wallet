package grpc

import (
	"context"
	"flag"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.ru/rkosykh/demo-wallet/internal/config"
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/domain"
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/storage"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"gitlab.ru/rkosykh/demo-wallet/test/utils/clients/grpc"
)

var (
	store        storage.Storage
	walletClient desc.DemoWalletClient
)

func TestMain(m *testing.M) {
	flag.Parse()

	ctx := context.Background()

	db, err := config.ConnectToPostgres(ctx, config.Database{
		Dsn:               os.Getenv(config.DSNKey),
		ConnTimeout:       time.Duration(10),
		MaxOpenConn:       50,
		MasterMaxIdleConn: 50,
		Driver:            "pgx",
		Migrations:        "migrations",
	})
	if err != nil {
		log.Fatal().Err(err).Msg("Can't init connect to DB")
	}
	defer db.Close()

	store = storage.New(db)

	client, clientConn := grpc.NewServiceClient()
	defer clientConn.Close()
	walletClient = client

	code := m.Run()
	os.Exit(code)
}

func mustCreateAccount(ctx context.Context, amount int32) (*domain.Account, error) {
	account := &domain.Account{
		AccountID:   uuid.NewString(),
		Amount:      amount,
		Description: "test account",
	}
	err := store.AddAccount(ctx, account)
	if err != nil {
		return nil, err
	}

	return account, nil
}
