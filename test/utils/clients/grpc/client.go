package grpc

import (
	"context"

	"github.com/rs/zerolog/log"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// NewServiceClient - create client for server with signInterceptor
func NewServiceClient() (desc.DemoWalletClient, *grpc.ClientConn) {
	dialOpt := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	}

	conn, err := grpc.DialContext(context.Background(),
		":8002", dialOpt...,
	)

	if err != nil {
		log.Fatal().Err(err).Msg("Failed to dial bufnet")
	}

	client := desc.NewDemoWalletClient(conn)

	return client, conn
}
