# Demo Wallet

## Run app
```shell
 # create db in docker-compose
 make dc-db-up
 # migration up
 make db-up
 # run app
 make run
 ```

## Run app in docker-compose
 ```shell
 make dc-app-up
 ```

## Run tests
```shell
# e2e-tests
make e2e
# unit-tests
make test
# unit-tests with coverage
make test-cov
```

## Generate gRPC server and client
```shell
made deps
make generate
```

## Links
 - [swagger](http://localhost:8080/swagger/)
 - [Victoria Metrics](http://localhost:8428)
 - [Grafana](http://localhost:3000)