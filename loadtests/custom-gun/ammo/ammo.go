package ammo

// Handler .
type Handler = string

// nolint
const (
	CreateAccount     Handler = "CreateAccount"
	GetAccountBalance         = "GetAccountBalance"
	Credit                    = "Credit"
	Debit                     = "Debit"
)

// Ammo .
type Ammo struct {
	Tag       Handler `json:"tag"`
	Amount    int32   `json:"amount"`
	AccountID string  `json:"accountId"`
}
