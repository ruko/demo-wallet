package ammo

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
	"gitlab.ru/rkosykh/custom_gun/utils"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
)

// Generate generate ammo
func Generate(ammoCount int64, ammoFile *os.File) error {
	ctx := context.Background()
	handlers := []Handler{
		CreateAccount,
		GetAccountBalance,
		Credit,
		Debit,
	}

	accounts, err := createAccounts(ctx, ammoCount)
	if err != nil {
		return err
	}

	for _, acc := range accounts {
		for _, handle := range handlers {
			ammo := &Ammo{
				Tag:       handle,
				AccountID: acc.GetAccountId(),
				Amount:    acc.GetAmount(),
			}
			addAmmoToFile(ammoFile, ammo)
		}
	}

	return nil
}

func createAccounts(ctx context.Context, count int64) ([]*desc.CreateAccountResponse, error) {
	target := "localhost:8002"
	conn, err := utils.InitDialContext(ctx, target)
	if err != nil {
		log.Fatal().Err(err).Msgf("Can't init connect to '%s'", target)
	}
	client := desc.NewDemoWalletClient(conn)

	accounts := make([]*desc.CreateAccountResponse, 0, count)
	for i := int64(0); i < count; i++ {
		resp, err := client.CreateAccount(ctx, &desc.CreateAccountRequest{
			Description: "custom gun ammo",
			Amount:      1000,
		})
		if err != nil {
			return nil, err
		}

		accounts = append(accounts, resp)
		if len(accounts)%100 == 0 {
			log.Info().Msgf("got +100 accounts [total %d]", len(accounts))
		}
	}
	log.Info().Msgf("total account: %d", len(accounts))

	return accounts, nil
}

func addAmmoToFile(ammoFile *os.File, ammo *Ammo) {
	b, err := json.Marshal(ammo)
	if err != nil {
		log.Fatal().Err(err).Msg("failed ammo generate")
	}
	row := fmt.Sprintf("%s\n", b)

	_, err = ammoFile.WriteString(row)
	if err != nil {
		log.Fatal().Err(err).Msg("failed add ammo to file")
	}

}
