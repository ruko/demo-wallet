package main

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"gitlab.ru/rkosykh/custom_gun/ammo"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
)

// CreateAccount .
func (g *Gun) CreateAccount(ctx context.Context, client desc.DemoWalletClient, ammo *ammo.Ammo) int {
	_, err := client.CreateAccount(ctx, &desc.CreateAccountRequest{
		Description: "pandora custom gun",
		Amount:      ammo.Amount,
	})

	if code := checkNoErr(err, ammo.Tag); code != http.StatusOK {
		return code
	}

	return http.StatusOK
}

// GetAccountBalance .
func (g *Gun) GetAccountBalance(ctx context.Context, client desc.DemoWalletClient, ammo *ammo.Ammo) int {
	resp, err := client.GetAccountBalance(ctx, &desc.GetAccountBalanceRequest{
		AccountId: ammo.AccountID,
	})

	if code := checkNoErr(err, ammo.Tag); code != http.StatusOK {
		return code
	}

	if resp.GetAmount() != ammo.Amount {
		return 201
	}

	return http.StatusOK
}

// Credit .
func (g *Gun) Credit(ctx context.Context, client desc.DemoWalletClient, ammo *ammo.Ammo) int {
	resp, err := client.Credit(ctx, &desc.CreditRequest{
		Amount:      ammo.Amount,
		AccountId:   ammo.AccountID,
		OperationId: uuid.NewString(),
	})

	if code := checkNoErr(err, ammo.Tag); code != http.StatusOK {
		return code
	}

	if resp.GetStatus() != desc.OperationStatus_STATUS_OK {
		return 211
	}

	return http.StatusOK
}

// Debit .
func (g *Gun) Debit(ctx context.Context, client desc.DemoWalletClient, ammo *ammo.Ammo) int {
	resp, err := client.Debit(ctx, &desc.DebitRequest{
		Amount:      ammo.Amount,
		AccountId:   ammo.AccountID,
		OperationId: uuid.NewString(),
	})

	if code := checkNoErr(err, ammo.Tag); code != http.StatusOK {
		return code
	}

	if resp.GetStatus() != desc.OperationStatus_STATUS_OK {
		return 221
	}

	return http.StatusOK
}
