package demowallet

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Credit списание денег
func (i *Implementation) Credit(ctx context.Context, req *desc.CreditRequest) (*desc.CreditResponse, error) {
	operationID := req.GetOperationId()
	if operationID == "" {
		operationID = uuid.NewString()
	}
	err := i.store.Credit(ctx, req.GetAccountId(), req.GetAmount(), operationID)
	if err != nil {
		return &desc.CreditResponse{
			Status: desc.OperationStatus_STATUS_FAIL,
		}, status.Error(codes.Internal, fmt.Sprintf("Credit process err: %v", err))
	}

	return &desc.CreditResponse{
		Status: desc.OperationStatus_STATUS_OK,
	}, nil
}
