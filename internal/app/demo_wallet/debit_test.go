package demowallet

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.ru/rkosykh/demo-wallet/internal/pkg/domain"
	desc "gitlab.ru/rkosykh/demo-wallet/pkg/api/demo-wallet"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestDebit(t *testing.T) {
	t.Run("Positive cases", func(t *testing.T) {
		ctx := context.Background()

		account, err := mustCreateAccount(ctx, 100)
		require.NoError(t, err)

		impl := Implementation{store: store}

		debitRequest := &desc.DebitRequest{
			AccountId:   account.AccountID,
			Amount:      100,
			OperationId: uuid.NewString(),
		}

		resp, err := impl.Debit(ctx, debitRequest)
		require.NoError(t, err)
		require.NotNil(t, resp)
		require.Equal(t, desc.OperationStatus_STATUS_OK, resp.Status)

		operations, err := store.GetAccountOperations(ctx, account.AccountID, 1000)
		require.NoError(t, err)
		require.Len(t, operations, 1)

		require.Equal(t, debitRequest.Amount, operations[0].Amount)
		require.Equal(t, debitRequest.OperationId, operations[0].OperationID)
		require.Equal(t, domain.OperationTypeDebit, operations[0].OperationType)

		checkNewAccountBalance(ctx, t, impl, account, debitRequest.Amount, domain.OperationTypeDebit)
	})

	t.Run("Negative cases", func(t *testing.T) {
		t.Run("Account doesn't exists", func(t *testing.T) {
			ctx := context.Background()

			impl := Implementation{store: store}

			accountID := uuid.NewString()
			resp, err := impl.Debit(ctx, &desc.DebitRequest{
				AccountId:   uuid.NewString(),
				Amount:      100,
				OperationId: uuid.NewString(),
			})
			require.EqualError(t, err, status.Error(codes.Internal, "Debit process err: can't get account balance for change amount: account not found").Error())
			require.NotNil(t, resp)
			require.Equal(t, desc.OperationStatus_STATUS_FAIL, resp.Status)

			checkOperationNotExists(ctx, t, accountID)
		})

		t.Run("Double operation with same ID", func(t *testing.T) {
			ctx := context.Background()

			account, err := mustCreateAccount(ctx, 100)
			require.NoError(t, err)

			impl := Implementation{store: store}

			debitRequest := &desc.DebitRequest{
				AccountId:   account.AccountID,
				Amount:      100,
				OperationId: uuid.NewString(),
			}

			t.Log("First credit operation")

			resp, err := impl.Debit(ctx, debitRequest)
			require.NoError(t, err)
			require.NotNil(t, resp)
			require.Equal(t, desc.OperationStatus_STATUS_OK, resp.Status)

			t.Log("Second credit operation")

			resp, err = impl.Debit(ctx, debitRequest)
			require.Error(t, err)
			require.NotNil(t, resp)
			require.Equal(t, desc.OperationStatus_STATUS_FAIL, resp.Status)

			operations, err := store.GetAccountOperations(ctx, account.AccountID, 1000)
			require.NoError(t, err)
			require.Len(t, operations, 1)
		})

	})
}

func checkNewAccountBalance(ctx context.Context, t *testing.T, impl Implementation,
	account *domain.Account, operationAmount int32, operationType domain.OperationType) {

	balance, err := impl.GetAccountBalance(ctx, &desc.GetAccountBalanceRequest{
		AccountId: account.AccountID,
	})
	require.NoError(t, err)
	require.NotNil(t, balance)

	expectedAmount := account.Amount + (int32(operationType) * operationAmount)
	require.Equal(t, expectedAmount, balance.Amount)
}

func checkOperationNotExists(ctx context.Context, t *testing.T, accountID string) {
	operations, err := store.GetAccountOperations(ctx, accountID, 1000)
	require.NoError(t, err)
	require.Len(t, operations, 0)
}
